/*
    Test FB video URLs:
    https://www.facebook.com/timo.halen/videos/10154224849558796/
    https://www.facebook.com/DavidAvocadoWolfe/videos/10153653600171512/
    https://www.facebook.com/BruhhOfficial/videos/608835499297845/
    https://www.facebook.com/brightside/videos/306720096342541/
*/

function isMobileDevice() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[#&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var UrlForm = React.createClass({
  getInitialState: function() {
    return {url: ''};
  },
  handleUrlChange: function(e) {
    this.setState({url: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var url = this.state.url.trim();
    if (!url) {
      return;
    }
    var shareUrl = window.location.href + '#url=' + url
    //window.location.href = shareUrl;
    this.props.onUrlSubmit(url, shareUrl);
    this.setState({url: ''});
  },
  render: function() {
    return (
      <form className="urlForm" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input
            type="url"
            placeholder="Facebook Video URL"
            value={this.state.url}
            onChange={this.handleUrlChange}
            className="form-control input-lg"
            id="videoUrlInput"
          />
        </div>
        <div className="form-group">
          <input type="submit" value="Post" className="btn btn-default input-lg" />
        </div>
      </form>
    );
  }
});
var FBVideo = React.createClass({
  componentWillReceiveProps: function(nextProps) {
    //console.log("componentWillReceiveProps: ");
    //console.log(nextProps);
  },
  handleCopyUrl: function(e) {
    e.preventDefault();
    var oUrlCopied = document.getElementById('urlCopied');
    if (!iOS) {
      oUrlCopied.style.display = 'inline';
    }
  },
  render: function() {
    if (this.props.video.url || this.props.video.shareUrl) {
      if (this.props.video.urlFromParam) {
        var autoPlay = "true";
      } else {
        var autoPlay = "false";
      }
      if (iOS) {
        var copyText = "Select URL";
      } else {
        var copyText = "Copy URL";
      }
      if (mobileDevice) {
        var whatsappUrl = "whatsapp://send?text=" + encodeURIComponent(this.props.video.shareUrl);
        var whatsappButton = <a href={whatsappUrl} className="whatsapp">&nbsp;</a>;
      } else {
        var whatsappButton = '';
      }
      return (
        <div id="video-wrapper">
          <div className="fb-video"
            data-href={this.props.video.url}
            data-allowfullscreen="true"
            data-width="auto"
            data-autoplay={autoPlay}>
          </div>
          <div id="share-section">
            <h2>Share</h2>
            <form className="shareForm" onSubmit={this.handleCopyUrl}>
              <div className="form-group">
                <input id="share-url" type="text" readOnly value={this.props.video.shareUrl} className="form-control input-lg" />
              </div>
              <div className="form-group">
                <input type="submit" value={copyText} id="copyUrlButton" className="btn btn-default input-lg" data-clipboard-target="#share-url" />{whatsappButton}<span id="urlCopied" className="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            </form>
          </div>
        </div>
      );
    } else {
      return false;
    }
  }
});
var VideoBox = React.createClass({
  getInitialState: function() {
    var urlFromParam = getParameterByName('url');
    if (urlFromParam) {
      return {url: urlFromParam, shareUrl: false, urlFromParam: true};
    } else {
      return {url: false, shareUrl: false, urlFromParam: false};
    }
  },
  componentDidMount: function() {
    //console.log("VideoBox rendered");
  },
  handleUrlSubmit: function(url, shareUrl) {
    //console.log("VideoBox.handleUrlSubmit");
    //console.log("url = " + url);
    //console.log("shareUrl = " + shareUrl);
    this.setState(
      {url: url, shareUrl: shareUrl, urlFromParam: false},
      function () {
        window.fbAsyncInit();
        new Clipboard('#copyUrlButton');
      }
    );
  },
  render: function() {
    if (this.state.urlFromParam) {
      var classes = "videoBox urlFromParam";
    } else {
      var classes = "videoBox";
    }
    return (
      <div className={classes} id={this.state.urlFromParam}>
      <h1><i className="fa fa-facebook-official" aria-hidden="true"></i> Video Player</h1>
      <UrlForm onUrlSubmit={this.handleUrlSubmit} />
      <FBVideo video={this.state} />
      </div>
    );
  }
});

var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var mobileDevice = isMobileDevice();
// Uncomment for mobile debugging
// mobileDevice = true;

ReactDOM.render(
  <VideoBox />,
  document.getElementById('videobox')
);
